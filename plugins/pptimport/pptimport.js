KindEditor.plugin('pptimport', function(K)
{
	var editor = this, name = 'pptimport';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        WordPaster.getInstance().SetEditor(editor);
        WordPaster.getInstance().importPPT();
	});
});
