KindEditor.plugin('pptpaster', function(K)
{
	var editor = this, name = 'pptpaster';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        WordPaster.getInstance().SetEditor(editor);
        WordPaster.getInstance().PastePPT();
	});
});
