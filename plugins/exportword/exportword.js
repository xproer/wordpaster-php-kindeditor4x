﻿KindEditor.lang({
    exportword: '导出Word文档（docx）'
});
KindEditor.plugin('exportword', function(K)
{
	var editor = this, name = 'exportword';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        window.zyOffice.SetEditor(editor).api.exportWord();
	});
});
