KindEditor.plugin('excelimport', function(K)
{
	var editor = this, name = 'excelimport';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        WordPaster.getInstance().SetEditor(editor);
        WordPaster.getInstance().importExcel();
	});
});
