﻿KindEditor.lang({
    importword: '导入Word文档（docx）'
});
KindEditor.plugin('importword', function(K)
{
	var editor = this, name = 'importword';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        window.zyOffice.SetEditor(editor).api.openDoc();
	});
});
