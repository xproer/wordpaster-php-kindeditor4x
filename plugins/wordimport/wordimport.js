KindEditor.plugin('wordimport', function(K)
{
	var editor = this, name = 'wordimport';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        WordPaster.getInstance().SetEditor(editor);
        WordPaster.getInstance().importWord();
	});
});
