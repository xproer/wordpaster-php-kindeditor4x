﻿KindEditor.lang({
    importpdf: '导入PDF文档'
});
KindEditor.plugin('importpdf', function(K)
{
	var editor = this, name = 'importpdf';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        window.zyOffice.SetEditor(editor).api.openPdf();
	});
});
