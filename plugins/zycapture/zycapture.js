﻿KindEditor.lang({
    zycapture: '截屏（zyCapture）'
});
KindEditor.plugin('zycapture', function(K)
{
	var editor = this, name = 'zycapture';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        window.zyCapture.setEditor(editor).Capture2();
	});
});
