KindEditor.plugin('importwordtoimg', function(K)
{
	var editor = this, name = 'importwordtoimg';
	// 点击图标时执行
	editor.clickToolbar(name, function()
	{
        WordPaster.getInstance().SetEditor(editor);
        WordPaster.getInstance().importWordToImg();
	});
});
