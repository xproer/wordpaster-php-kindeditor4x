<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<!--此页面实现Word图片自动批量上传的功能-->
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>WordPaster For KindEditor-4.x</title>
    <link type="text/css" rel="Stylesheet" href="demo.css" />
    <link type="text/css" rel="Stylesheet" href="WordPaster/js/skygqbox.css" />
	<link type="text/css" rel="stylesheet" href="themes/simple/simple.css" />
	<script type="text/javascript" charset="utf-8" src="kindeditor-min.js"></script>
	<script type="text/javascript" charset="utf-8" src="lang/zh_CN.js"></script>
    <script type="text/javascript" src="WordPaster/js/json2.min.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/jquery-1.4.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="WordPaster/js/skygqbox.js" charset="utf-8"></script>
    <script type="text/javascript" src="WordPaster/js/w.js" charset="utf-8"></script>
	<script type="text/javascript" src="zyCapture/z.js" charset="utf-8"></script>
    <script type="text/javascript" src="zyOffice/js/o.js" charset="utf-8"></script>
	<script type="text/javascript" src="vue.min.js" charset="utf-8"></script>
	<script type="text/javascript" src="demo.js" charset="utf-8"></script>
</head>
<body>
	<div id="demos"></div>	
	<div id="kind">
        <kindeditor ref="kind"></kindeditor>
    </div>
    <script>
        Vue.component('kindeditor', {
            data: function(){
                return {
                    editor: null
                }
            },
            props: {
                value: '',
                config: {}
            },
            mounted: function(){
                var editor;
                var pos = window.location.href.lastIndexOf("/");
				var api = [
					window.location.href.substr(0, pos + 1),
					"php/upload.php"
				].join("");
                WordPaster.getInstance({
					//上传接口：http://www.ncmem.com/doc/view.aspx?id=d88b60a2b0204af1ba62fa66288203ed
					PostUrl: api,
					//为图片地址增加域名：http://www.ncmem.com/doc/view.aspx?id=704cd302ebd346b486adf39cf4553936
					ImageUrl: "",
					//设置文件字段名称：http://www.ncmem.com/doc/view.aspx?id=c3ad06c2ae31454cb418ceb2b8da7c45
					FileFieldName: "file",
					//提取图片地址：http://www.ncmem.com/doc/view.aspx?id=07e3f323d22d4571ad213441ab8530d1
					ImageMatch: '',
					event:{
						dataReady:function(e){
							//e.word,
							//e.imgs:tag1,tag2,tag3
							console.log(e.imgs)
						}
					}
				});//加载控件
		
                //zyCapture
                zyCapture.getInstance({
                    config: {
                        PostUrl: api,
                        FileFieldName: "file",
                        Fields: { uname: "test" }
                    }
                });

                //zyoffice
                //使用前请在服务端部署zyOffice，
                //http://www.ncmem.com/doc/view.aspx?id=82170058de824b5c86e2e666e5be319c
                zyOffice.getInstance({
                    word:"http://localhost:13710/zyoffice/word/convert",
                    wordExport:"http://localhost:13710/zyoffice/word/export",
                    pdf:"http://localhost:13710/zyoffice/pdf/upload"
                });

                KindEditor.ready(function (K) {
                    editor = K.create('#editor'
                        ,
                        {
                            items: [
                                'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'cut', 'copy', 'paste',
                                'plainpaste', 'wordpaste',
                                '|',
                                'zycapture',
                                '|',
                                'wordpaster','importwordtoimg','netpaster','wordimport','excelimport','pptimport','pdfimport', '|', 
                                'importword','exportword','importpdf','|',
                                'justifyleft', 'justifycenter', 'justifyright',
                                'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                                'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                                'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                                'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image',
                                'flash', 'media', 'insertfile', 'table', 'hr', 'emoticons', 'map', 'code', 'pagebreak',
                                'link', 'unlink', '|', 'about']
                            , afterCreate: function () {
                                WordPaster.getInstance().SetEditor(this);
                                window.zyCapture.setEditor(this);
                                window.zyOffice.SetEditor(this);
                                var self = this;
                                //自定义 Ctrl + V 事件。
                                KindEditor.ctrl(self.edit.doc, 'V', function () { WordPaster.getInstance().Paste(); });
                            }
                        });
                    editor.addContextmenu({
                        title: '粘贴',
                        click: function () {
                            WordPaster.getInstance().Paste();
                            editor.hideMenu();
                        },
                        cond: function () { return true; },
                        width: 150,
                    });
                    // 插入分割线
                    //editor.addContextmenu({ title: '-' });
                });
            },
            methods: {},
            destroyed: function(){ },
            template: '<div><textarea id="editor" name="editor" style="width:100%;height:300px;visibility:hidden;"/><p>泽优全平台内容发布解决方案 for php kindeditor4</p><p>泽优Word一键粘贴控件（WordPaster）</p><p>泽优全平台截屏控件（zyCapture）</p><p>泽优Office文档转换服务（zyOffice）</p></div>'
        });

        var kind = new Vue({
            el: '#kind',
            data: {
            }
            , mounted: function () {
            }
        });
    </script>
</body>
</html>